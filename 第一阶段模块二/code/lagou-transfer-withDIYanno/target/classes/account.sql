/*
 Navicat Premium Data Transfer

 Source Server         : funfish
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : edu_mybatis

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 19/10/2020 17:56:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `name` varchar(255) DEFAULT NULL,
  `cardNo` varbinary(255) DEFAULT NULL,
  `money` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of account
-- ----------------------------
BEGIN;
INSERT INTO `account` VALUES ('韩梅梅', 0x36303239363231303131303030, 994);
INSERT INTO `account` VALUES ('李大雷', 0x36303239363231303131303031, 1006);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
