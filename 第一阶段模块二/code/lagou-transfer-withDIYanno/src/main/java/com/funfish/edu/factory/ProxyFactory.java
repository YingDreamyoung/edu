package com.funfish.edu.factory;

import com.funfish.edu.anno.FunTransactional;
import com.funfish.edu.utils.TransactionManager;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * @author 应癫
 *
 *
 * 代理对象工厂：生成代理对象的
 */

public class ProxyFactory {


    private TransactionManager transactionManager;

    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /*private ProxyFactory(){

    }

    private static ProxyFactory proxyFactory = new ProxyFactory();

    public static ProxyFactory getInstance() {
        return proxyFactory;
    }*/



    /**
     *  获取代理
     * @param obj  委托对象
     * @return   代理对象
     */
    public Object getProxy(Object obj) {

        // 获取代理对象
        // 根据条件判断 对象代理对象的方式
        if(isImpleInterface(obj)){
          return  getJdkProxy(obj);
        }else {
          return  getCglibProxy(obj);
        }

    }


    /**
     * Jdk动态代理
     * @param obj  委托对象
     * @return   代理对象
     */
    public Object getJdkProxy(Object obj) {

        // 获取代理对象
        return  Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        // 判断 该方法上是否存在事务注解
                        // 代理之后方法上的注解，获取不到
                        FunTransactional declaredAnnotation = obj.getClass().getMethod(method.getName(), method.getParameterTypes()).getDeclaredAnnotation(FunTransactional.class);
                        if(declaredAnnotation !=null){
                            System.out.println("doWithTransaction = ");
                            result = doWithTransaction(method,obj,args);
                        }else {
                            System.out.println("invoke = ");
                            result = method.invoke(obj,args);
                        }

                        return result;
                    }
                });

    }


    /**
     * 使用cglib动态代理生成代理对象
     * @param obj 委托对象
     * @return
     */
    public Object getCglibProxy(Object obj) {
        return  Enhancer.create(obj.getClass(), new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                Object result = null;

                // 判断 该方法上是否存在事务注解
                if(method.getAnnotation(FunTransactional.class) !=null){
                    System.out.println("method.getAnnotation(FunTransactional.class) = " + method.getAnnotation(FunTransactional.class));
                    result = doWithTransaction(method,obj,objects);
                }else {
                    result = method.invoke(obj,objects);
                }
                return result;
            }
        });
    }


    private Object doWithTransaction(Method method,Object obj, Object[] args) throws SQLException, InvocationTargetException, IllegalAccessException {

        Object result = null;

        try{
            // 开启事务(关闭事务的自动提交)
            transactionManager.beginTransaction();
            result = method.invoke(obj,args);
            // 提交事务
            transactionManager.commit();
        }catch (Exception e) {
            e.printStackTrace();
            // 回滚事务
            transactionManager.rollback();
            // 抛出异常便于上层servlet捕获
            throw e;
        }

        return result;
    }

    // 判断是否存在指定的注解
    private Boolean isAnnotationPresentFun(Method method,Class anaoClass){

        Class clazz = method.getClass();
        System.out.println("clazz = " + clazz);
        System.out.println("anaoClass = " + anaoClass);
        Annotation annotation = method.getAnnotation(anaoClass);
        System.out.println("annotation = " + annotation);

        if(annotation !=null){
            return true;
        }
        return false;
    }

    // 判断是否实现了接口方法
    private Boolean isImpleInterface(Object obj){

        Class<?> objClass = obj.getClass();
        Class<?>[] interfaces = objClass.getInterfaces();
        if(interfaces !=null){

            Boolean isImpl = false;
            // 自身方法
            Method[] declaredSelfMethods = objClass.getDeclaredMethods();
            // 接口方法
            Method[] declaredAllInfaceMethods = new Method[16];
            for (Class<?> anInterface : interfaces) {
                Method[] declaredMethods = anInterface.getDeclaredMethods();
                Method[] methods = Arrays.copyOf(declaredAllInfaceMethods, declaredAllInfaceMethods.length + declaredMethods.length);
                System.arraycopy(declaredMethods, 0, methods, declaredAllInfaceMethods.length, declaredMethods.length);
                declaredAllInfaceMethods = methods;
            }

            System.out.println("declaredAllInfaceMethods = " + declaredAllInfaceMethods);
            System.out.println("declaredAllInfaceMethods.length = " + declaredAllInfaceMethods.length);

            for (Method method : declaredSelfMethods) {
                // 该方法是否实现了接口
                boolean methodImpl = false;
                for (Method inface : declaredAllInfaceMethods) {
                    if(inface == null)
                        continue;
                    System.out.println("inface = " + inface);
                    if(method.getName().equals(inface.getName())){
                        methodImpl = true;
                    }
                }
                if(!methodImpl){
                    return false;
                }
            }
            return true;

        }else {
            return false;
        }

    }

}
