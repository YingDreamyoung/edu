package com.funfish.edu.anno;

import java.lang.annotation.*;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-18 17:33
 **/
@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FunAutowired {

    String value() default "";
}




