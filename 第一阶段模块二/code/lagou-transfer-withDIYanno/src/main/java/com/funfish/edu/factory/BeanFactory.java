package com.funfish.edu.factory;

import com.funfish.edu.anno.FunAutowired;
import com.funfish.edu.anno.FunService;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 应癫
 *
 * 工厂类，生产对象（使用反射技术）
 */
public class BeanFactory {

    /**
     * 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
     * 任务二：对外提供获取实例对象的接口（根据id获取）
     */

    private static Map<String,Object> map = new HashMap<>();  // 存储对象


    static {
        // 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
        // 加载xml
        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
        // 解析xml
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            List<Element> beanList = rootElement.selectNodes("//bean");
            for (int i = 0; i < beanList.size(); i++) {
                Element element =  beanList.get(i);
                // 处理每个bean元素，获取到该元素的id 和 class 属性
                String id = element.attributeValue("id");        // accountDao
                String clazz = element.attributeValue("class");  // JdbcAccountDaoImpl
                // 通过反射技术实例化对象
                Class<?> aClass = Class.forName(clazz);
                Object o = aClass.newInstance();  // 实例化之后的对象

                // 存储到map中待用
                map.put(id,o);

            }


            Element scanElement = (Element) rootElement.selectObject("//component-scan");
                // 扫描包下的注解
            // 目前就假定只有一个包路径
            String backageName = scanElement.attributeValue("base-package");
            // 包名转换
            String splashPath = backageName.replaceAll("\\.", "/");
            System.out.println("splashPath = " + splashPath);
            //file:/D:/WorkSpace/java/ScanTest/target/classes/com/scan
            URL url = BeanFactory.class.getClassLoader().getResource(splashPath);
            System.out.println("url = " + url);
            String filePath = url.getFile();
            System.out.println("filePath = " + filePath);
            // 获取文件名
            List<String> names = readFromDirectory(filePath);

            // FunService 自定义注解
            for (String name : names) {
                if(name.endsWith(".class")){
                    String fullClassName = toFullyQualifiedName(name, backageName);
                    // 通过反射技术实例化对象
                    Class<?> aClass = Class.forName(fullClassName);
                    // 判断 该类是否 满足自定义注解条件
                    boolean annotationPresent = aClass.isAnnotationPresent(FunService.class);
                    if(annotationPresent){
                        String keyId = "";
                        // 获取自定义注解对象
                        FunService annotation = aClass.getAnnotation(FunService.class);
                        // 根据对象获取注解值
                        String value = annotation.value();
                        // 获取id
                        if(value.trim().isEmpty()){
                           // 没有设置注解
                            // 默认实现
                            // 文件名首字母小写
                            keyId = toLowerCaseFirstOne(trimExtension(name));
                        }else {
                            keyId = value;
                        }
                        Object o = aClass.newInstance();  // 实例化之后的对象
                        // 存储到map中待用
                        map.put(keyId,o);

                        // 这里为了实现功能 只做了一个基本实现
                        // FunService 自定义注解

                        // 判断 该类是否 满足自定义注解条件 FunAutowired
                        Field[] declaredFields = aClass.getDeclaredFields();
                        for (Field field : declaredFields) {
                            field.setAccessible(true);
                            annotationPresent = field.isAnnotationPresent(FunAutowired.class);
                            if(annotationPresent){
                                Class<?> type = field.getType();
                                // 遍历 该类的的实现 是否在在 map 中
                                for(Map.Entry<String, Object> entry : map.entrySet()){
                                    String mapKey = entry.getKey();
                                    Object mapValue = entry.getValue();
                                    boolean instance = type.isInstance(mapValue);
                                    if(instance){
                                        // 是其实现类 就注入
                                        field.set(o,mapValue);
                                    }
//                                mapValue.getClass().isAssignableFrom(type);

                                }
                                // 存储到map中待用
//                                map.put(keyId,o);
                            }
                        }

                    }
                }
            }


            // 实例化完成之后维护对象的依赖关系，检查哪些对象需要传值进入，根据它的配置，我们传入相应的值
            // 有property子元素的bean就有传值需求
            List<Element> propertyList = rootElement.selectNodes("//property");
            // 解析property，获取父元素
            for (int i = 0; i < propertyList.size(); i++) {
                Element element =  propertyList.get(i);   //<property name="AccountDao" ref="accountDao"></property>
                String name = element.attributeValue("name");
                String ref = element.attributeValue("ref");

                // 找到当前需要被处理依赖关系的bean
                Element parent = element.getParent();

                // 调用父元素对象的反射功能
                String parentId = parent.attributeValue("id");
                Object parentObject = map.get(parentId);
                // 遍历父对象中的所有方法，找到"set" + name
                Method[] methods = parentObject.getClass().getMethods();
                for (int j = 0; j < methods.length; j++) {
                    Method method = methods[j];
                    if(method.getName().equalsIgnoreCase("set" + name)) {  // 该方法就是 setAccountDao(AccountDao accountDao)
                        method.invoke(parentObject,map.get(ref));
                    }
                }

                // 把处理之后的parentObject重新放到map中
                map.put(parentId,parentObject);

            }




            // 开始扫描自定义注解

            // funservice
            // funautowore
            // funtransiton


        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }


    // 任务二：对外提供获取实例对象的接口（根据id获取）
    public static  Object getBean(String id) {
        return map.get(id);
    }

    private static List<String> readFromDirectory(String path) {

         File file = new File(path);
         String[] names = file.list();

         if (null == names) {
                 return null;
         }

         return Arrays.asList(names);
    }

    // 全类名组装 fullclassname
    private static String toFullyQualifiedName(String shortName, String basePackage) {

         StringBuilder sb = new StringBuilder(basePackage);
         sb.append('.');
         sb.append(trimExtension(shortName));
         //打印出结果
         System.out.println(sb.toString());
         return sb.toString();
     }

    /**
       * "Apple.class" -> "Apple"
       */
    // 文件名 转 简称类名
     public static String trimExtension(String name) {

         int pos = name.indexOf('.');
         if (-1 != pos) {
             return name.substring(0, pos);
         }

         return name;
     }

    public static String toLowerCaseFirstOne(String s){

        return Character.isLowerCase(s.charAt(0)) ? s : Character.toLowerCase(s.charAt(0)) + s.substring(1);

    }


}
