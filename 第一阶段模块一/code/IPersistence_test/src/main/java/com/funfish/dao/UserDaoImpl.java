//package com.funfish.dao;
//
//import com.funfish.io.Resources;
//import com.funfish.pojo.User;
//import com.funfish.sqlSession.SqlSession;
//import com.funfish.sqlSession.SqlSessionFactory;
//import com.funfish.sqlSession.SqlSessionFactoryBuilder;
//
//import java.io.InputStream;
//import java.util.List;
//
///**
// * @program: eduCoding
// * @description:
// * @author: funfish
// * @create: 2020-10-01 21:36
// **/
//public class UserDaoImpl implements IUserDao{
//
//
//    @Override
//    public User findByCondition(User user) throws Exception {
//
//        InputStream resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
//        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//
//        User selUser = sqlSession.selectOne("user.selectOne",user);
//
//        System.out.println("selUser = " + selUser);
//        return selUser;
//    }
//
//    @Override
//    public List<User> findAll() throws Exception {
//
//        InputStream resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
//        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//
//        List<User> objects = sqlSession.selectList("user.findAll");
//        System.out.println("objects = " + objects);
//        return objects;
//    }
//}
