package com.funfish.dao;

import com.funfish.pojo.User;

import java.util.List;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 21:32
 **/
public interface IUserDao {

    public User findByCondition(User user) throws Exception;

    public List<User> findAll() throws Exception;

    public int insert(User user) throws Exception;

    public int delete(User user) throws Exception;

    public int update(User user) throws Exception;
}
