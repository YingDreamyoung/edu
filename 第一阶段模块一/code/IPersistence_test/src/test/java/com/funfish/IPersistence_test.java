package com.funfish;

import com.funfish.dao.IUserDao;
import com.funfish.io.Resources;
import com.funfish.pojo.User;
import com.funfish.sqlSession.SqlSession;
import com.funfish.sqlSession.SqlSessionFactory;
import com.funfish.sqlSession.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;

/**
 * @program: eduCoding
 * @description:
 * @author: com.funfish.pojo
 * @create: 2020-10-01 14:09
 **/
public class IPersistence_test {

    @Test
    public void test() throws Exception {

        InputStream resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        IUserDao userDao  = sqlSession.getMapper(IUserDao.class);
        List<User> all = userDao.findAll();
        System.out.println("all = " + all);


        Long userId = 8L;
        /// insert
        User user = new User();
        user.setId(userId);
        user.setUsername("lucy");
//        int insert = userDao.insert(user);
//        System.out.println("insert = " + insert);

//        /// update
        user.setId(userId);
        user.setUsername("lucyupdated");
//        int update = userDao.update(user);
//        System.out.println("update = " + update);
//
////        /// delete
        user.setId(userId);
//        user.setUsername("lucyupdated");
        int delete = userDao.delete(user);
//
        System.out.println("delete = " + delete);




//
//        System.out.println("selUser = " + selUser);
//
//        List<Object> objects = sqlSession.selectList("user.selectList");
//        System.out.println("objects = " + objects);


    }


}
