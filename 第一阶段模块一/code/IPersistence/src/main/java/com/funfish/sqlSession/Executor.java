package com.funfish.sqlSession;

import com.funfish.pojo.Configuation;
import com.funfish.pojo.MappedStatement;

import java.sql.SQLException;
import java.util.List;

public interface Executor {

    public <E>List query(Configuation configuation, MappedStatement mappedStatement,Object... param) throws SQLException, Exception;
    public int insert(Configuation configuation, MappedStatement mappedStatement,Object... param) throws SQLException, Exception;
    public int delete(Configuation configuation, MappedStatement mappedStatement,Object... param) throws SQLException, Exception;
    public int update(Configuation configuation, MappedStatement mappedStatement,Object... param) throws SQLException, Exception;
}
