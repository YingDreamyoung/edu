package com.funfish.sqlSession;

public interface SqlSessionFactory {

    public SqlSession openSession();
}
