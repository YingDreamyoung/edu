package com.funfish.config;

import com.funfish.pojo.Configuation;
import com.funfish.pojo.MappedStatement;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 15:20
 **/
public class XmlMapperBuilder {

    private Configuation configuation;

    public XmlMapperBuilder(Configuation configuation) {
        this.configuation= configuation;
    }

    public void parse(InputStream inputStream) throws DocumentException {

        Document read = new SAXReader().read(inputStream);

        Element rootElement = read.getRootElement();
        String namespace = rootElement.attributeValue("namespace");
        List<Element> list = rootElement.selectNodes("select");
        list.addAll(rootElement.selectNodes("insert"));
        list.addAll(rootElement.selectNodes("delete"));
        list.addAll(rootElement.selectNodes("update"));
        for (Element element : list) {

            String id = element.attributeValue("id");
            String resultType = element.attributeValue("resultType");
            String paramerType = element.attributeValue("paramterType");
            String sqlText = element.getTextTrim();
            MappedStatement mappedStatement = new MappedStatement();
            mappedStatement.setId(id);
            mappedStatement.setParamterType(paramerType);
            mappedStatement.setResultType(resultType);
            mappedStatement.setSql(sqlText);

            configuation.getMappedStatementMap().put(namespace + "." + id,mappedStatement);

        }

    }
}
