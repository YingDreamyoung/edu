package com.funfish.sqlSession;

import com.funfish.config.BoundSql;
import com.funfish.pojo.Configuation;
import com.funfish.pojo.MappedStatement;
import com.funfish.utils.GenericTokenParser;
import com.funfish.utils.ParameterMapping;
import com.funfish.utils.ParameterMappingTokenHandler;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 17:47
 **/
public class SimpleExecutor implements Executor {

    @Override
    public <E> List query(Configuation configuation, MappedStatement mappedStatement, Object... param) throws Exception {

//        1、 注册驱动，获取连接
        Connection connection = configuation.getDataSource().getConnection();
//        2\ 获取sql 语句 #{id} -> ? ,并对值解析存储
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        // 3.获取预处理对象 preparedStatement
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSql());
        // 4 设置参数
        String paramterType = mappedStatement.getParamterType();

        Class<?> paramerTypeClass = getClassType(paramterType);



        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            String content = parameterMapping.getContent();
            // 反射
            Field declaredField = paramerTypeClass.getDeclaredField(content);
            // 暴力访问
            declaredField.setAccessible(true);
            Object o = declaredField.get(param[0]);
            preparedStatement.setObject(i+1,o);

        }
        // 5 执行sql
        ResultSet resultSet = preparedStatement.executeQuery();
        // 6 封装结果集
        String resultType = mappedStatement.getResultType();
        Class<?> resultTypeClass = getClassType(resultType);

        ArrayList<Object> objects = new ArrayList<>();

        while (resultSet.next()){
            // 元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            Object o = resultTypeClass.newInstance();
            for (int i = 1; i < metaData.getColumnCount(); i++) {
                // 列名
                String columnName = metaData.getColumnName(i);
                // 字段值
                Object value = resultSet.getObject(columnName);

                // 使用反射或者内省，根据数据库表 和 实体 完成数据封装
                PropertyDescriptor propertyDescriptor= new PropertyDescriptor(columnName,resultTypeClass);
                Method writeMethod = propertyDescriptor.getWriteMethod();
                writeMethod.invoke(o,value);
            }
            objects.add(o);
        }

        return objects;
    }


    @Override
    public int insert(Configuation configuation, MappedStatement mappedStatement, Object... param) throws Exception {

//        1、 注册驱动，获取连接
        Connection connection = configuation.getDataSource().getConnection();
//        2\ 获取sql 语句 #{id} -> ? ,并对值解析存储
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        // 3.获取预处理对象 preparedStatement
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSql());
        // 4 设置参数
        String paramterType = mappedStatement.getParamterType();

        Class<?> paramerTypeClass = getClassType(paramterType);

        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            String content = parameterMapping.getContent();
            // 反射
            Field declaredField = paramerTypeClass.getDeclaredField(content);
            // 暴力访问
            declaredField.setAccessible(true);
            Object o = declaredField.get(param[0]);
            preparedStatement.setObject(i+1,o);

        }
        // 5 执行sql
        int execute = preparedStatement.executeUpdate();
        return execute;

    }

    @Override
    public int delete(Configuation configuation, MappedStatement mappedStatement, Object... param) throws Exception {

//        1、 注册驱动，获取连接
        Connection connection = configuation.getDataSource().getConnection();
//        2\ 获取sql 语句 #{id} -> ? ,并对值解析存储
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        // 3.获取预处理对象 preparedStatement
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSql());
        // 4 设置参数
        String paramterType = mappedStatement.getParamterType();

        Class<?> paramerTypeClass = getClassType(paramterType);

        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            String content = parameterMapping.getContent();
            // 反射
            Field declaredField = paramerTypeClass.getDeclaredField(content);
            // 暴力访问
            declaredField.setAccessible(true);
            Object o = declaredField.get(param[0]);
            preparedStatement.setObject(i+1,o);

        }
        // 5 执行sql
       return  preparedStatement.executeUpdate();

    }

    @Override
    public int update(Configuation configuation, MappedStatement mappedStatement, Object... param) throws Exception {

//        1、 注册驱动，获取连接
        Connection connection = configuation.getDataSource().getConnection();
//        2\ 获取sql 语句 #{id} -> ? ,并对值解析存储
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        // 3.获取预处理对象 preparedStatement
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSql());
        // 4 设置参数
        String paramterType = mappedStatement.getParamterType();

        Class<?> paramerTypeClass = getClassType(paramterType);

        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            String content = parameterMapping.getContent();
            // 反射
            Field declaredField = paramerTypeClass.getDeclaredField(content);
            // 暴力访问
            declaredField.setAccessible(true);
            Object o = declaredField.get(param[0]);
            preparedStatement.setObject(i+1,o);

        }
        // 5 执行sql
        return preparedStatement.executeUpdate();

    }



    private Class<?> getClassType(String paramterType) throws ClassNotFoundException {

        if(paramterType!=null){
            Class<?> aClass = Class.forName(paramterType);
            return aClass;
        }
        return null;
    }

    private BoundSql getBoundSql(String sql) {

        // 占位符 -> ？
        // 变量填充
        ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();
        GenericTokenParser tokenParser = new GenericTokenParser("#{","}",parameterMappingTokenHandler);
        String parseSql = tokenParser.parse(sql);
        // #{} 里面的参数名称
        List<ParameterMapping> parameterMappings = parameterMappingTokenHandler.getParameterMappings();

        BoundSql boundSql = new BoundSql(parseSql, parameterMappings);
        return boundSql;
    }
}
