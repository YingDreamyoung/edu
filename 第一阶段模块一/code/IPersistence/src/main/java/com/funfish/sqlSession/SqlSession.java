package com.funfish.sqlSession;

import java.util.List;

public interface SqlSession {

    //    查询所有
    public <E> List<E> selectList(String mappedid,Object... params) throws Exception;

    public <T> T selectOne(String mappedid,Object... params) throws Exception;

    public int insert(String mappedid,Object... params) throws Exception;

    public int delete(String mappedid,Object... params) throws Exception;

    public int update(String mappedid,Object... params) throws Exception;

    // 为dao 接口实现代理类
    public <T> T getMapper(Class<?> mapperClass);
}
