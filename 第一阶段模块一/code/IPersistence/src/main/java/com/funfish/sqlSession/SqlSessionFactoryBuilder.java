package com.funfish.sqlSession;


import com.funfish.config.XmlConfigBuilder;
import com.funfish.pojo.Configuation;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 14:29
 **/
public class SqlSessionFactoryBuilder {


    public SqlSessionFactory build(InputStream inputStream) throws PropertyVetoException, DocumentException {

//        1、 使用dom4j 解析配置文件，将解析出来的内容封装到Configuaton 中

        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder();
        Configuation configuation = xmlConfigBuilder.parseConfig(inputStream);

//        2、 创建SqlSessionFactory  生产sqlSession : 会话对象

        DefaultSqlSessionFactory defaultSqlSessionFactory = new DefaultSqlSessionFactory(configuation);


        return defaultSqlSessionFactory;
    }

}
