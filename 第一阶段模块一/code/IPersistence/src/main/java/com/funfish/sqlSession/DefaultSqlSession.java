package com.funfish.sqlSession;

import com.funfish.pojo.Configuation;
import com.funfish.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 15:41
 **/
public class DefaultSqlSession implements SqlSession {

    private Configuation configuation;

    public DefaultSqlSession(Configuation configuation) {
        this.configuation = configuation;
    }


    /*增*/
    @Override
    public int insert(String mappedid, Object... params) throws Exception {

        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuation.getMappedStatementMap().get(mappedid);
        return simpleExecutor.insert(configuation, mappedStatement, params);
    }
    /*删*/
    @Override
    public int delete(String mappedid, Object... params) throws Exception {

        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuation.getMappedStatementMap().get(mappedid);
        return  simpleExecutor.delete(configuation, mappedStatement, params);
    }
    /*改*/
    @Override
    public int update(String mappedid, Object... params) throws Exception {

        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuation.getMappedStatementMap().get(mappedid);
        return simpleExecutor.update(configuation, mappedStatement, params);
    }

    @Override
    public <E> List<E> selectList(String mappedid, Object... params) throws Exception {

        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuation.getMappedStatementMap().get(mappedid);
        List<Object> list = simpleExecutor.query(configuation, mappedStatement, params);
        return (List<E>) list;
    }

    @Override
    public <T> T selectOne(String mappedid, Object... params) throws Exception {

        List<Object> objects = selectList(mappedid, params);
        if(objects.size()==1){
            return (T)objects.get(0);
        }else {
            throw new RuntimeException("返回多个结果");
        }
    }

    @Override
    public <T> T getMapper(Class<?> mapperClass) {

        // 使用JDK动态代理 来为dao 层实现代理类 ，并返回
        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                // 参数准备 1 statementid sql 唯一标识： namespace.id  = 接口全限定名 . 方法名
                // 方法名 findAll
                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();

                String statementId = className + "." +methodName;
                // 准备参数2 params
                // 获取背调用的返回值类型
                Type genericReturnType = method.getGenericReturnType();

                //这里采用方法关键字简单的映射,如果考虑实际情况，就需要做更多的关键信息参考
                if(methodName.contains("insert")){
                    return insert(statementId,args);
                }else if(methodName.contains("delete")){
                    return delete(statementId,args);
                }else if(methodName.contains("update")){
                    return update(statementId,args);
                }else if(genericReturnType instanceof ParameterizedType){
                    // 是否进行了泛型类型参数化
                    return selectList(statementId,args);
                }else {
                    return selectOne(statementId,args);
                }

            }
        });

        return (T) proxyInstance;
    }
}
