package com.funfish.pojo;

/**
 * @program: eduCoding
 * @description:
 * @author: com.funfish.pojo
 * @create: 2020-10-01 14:15
 **/
public class MappedStatement {

//    id
    private String id;
//    返回值
    private String resultType;
//    参数值
    private String paramterType;
//    sql 语句
    private String sql;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public String getParamterType() {
        return paramterType;
    }

    public void setParamterType(String paramterType) {
        this.paramterType = paramterType;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
