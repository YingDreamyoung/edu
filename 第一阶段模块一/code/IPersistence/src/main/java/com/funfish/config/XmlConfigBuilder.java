package com.funfish.config;

import com.funfish.io.Resources;
import com.funfish.pojo.Configuation;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 14:36
 **/
public class XmlConfigBuilder {

    private Configuation configuation;

    public XmlConfigBuilder() {
        configuation = new Configuation();
    }

    public Configuation parseConfig(InputStream inputStream) throws DocumentException, PropertyVetoException {

//        dom4j
        Document read = new SAXReader().read(inputStream);
//        <configuration>
        Element rootElement = read.getRootElement();
        List<Element> list = rootElement.selectNodes("//property");
        Properties properties = new Properties();
        for (Element element : list) {
            String name = element.attributeValue("name");
            String value = element.attributeValue("value");
            properties.setProperty(name,value);
        }

        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        comboPooledDataSource.setDriverClass(properties.getProperty("driverClass"));
        comboPooledDataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
        comboPooledDataSource.setUser(properties.getProperty("user"));
        comboPooledDataSource.setPassword(properties.getProperty("password"));

        configuation.setDataSource(comboPooledDataSource);

//        mapper.xml 解析
        List<Element> mapperList = rootElement.selectNodes("//mapper");

        for (Element element : mapperList) {
            String resource = element.attributeValue("resource");
            InputStream resourceAsStream = Resources.getResourceAsStream(resource);
            XmlMapperBuilder xmlMapperBuilder = new XmlMapperBuilder(configuation);
            xmlMapperBuilder.parse(resourceAsStream);

        }

        return configuation;
    }
}
