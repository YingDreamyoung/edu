package com.funfish.config;

import com.funfish.utils.ParameterMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 18:08
 **/
public class BoundSql {

    private String sql;
    private List<ParameterMapping> parameterMappingList =new ArrayList<>();

    public BoundSql(String sql, List<ParameterMapping> parameterMappingList) {
        this.sql = sql;
        this.parameterMappingList = parameterMappingList;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<ParameterMapping> getParameterMappingList() {
        return parameterMappingList;
    }

    public void setParameterMappingList(List<ParameterMapping> parameterMappingList) {
        this.parameterMappingList = parameterMappingList;
    }
}
