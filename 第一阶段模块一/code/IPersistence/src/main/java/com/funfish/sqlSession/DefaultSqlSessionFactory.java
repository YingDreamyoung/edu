package com.funfish.sqlSession;

import com.funfish.pojo.Configuation;

/**
 * @program: eduCoding
 * @description:
 * @author: funfish
 * @create: 2020-10-01 15:35
 **/
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuation configuation;

    public DefaultSqlSessionFactory(Configuation configuation) {
        this.configuation = configuation;
    }

    @Override
    public SqlSession openSession() {

        return new DefaultSqlSession(configuation);
    }
}
