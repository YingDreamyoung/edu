package com.funfish.io;

import java.io.InputStream;

/**
 * @program: eduCoding
 * @description:
 * @author: com.funfish.pojo
 * @create: 2020-09-23 23:25
 **/
public class Resources {

//    根据配置文件的，将配置文件加载成字节输入流，存放在内容当中
    public static InputStream getResourceAsStream(String path){

        InputStream resourceAsStream = Resources.class.getClassLoader().getResourceAsStream(path);
//        Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
        return resourceAsStream;

    }

}
