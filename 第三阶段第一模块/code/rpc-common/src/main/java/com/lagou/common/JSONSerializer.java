package com.lagou.common;

import com.alibaba.fastjson.JSON;

/**
 * @program: rpc-common
 * @description:
 * @author: funfish
 * @create: 2020-11-22 14:18
 **/
public class JSONSerializer implements Serializer {

    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }



    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);

    }
}
