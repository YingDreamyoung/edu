package com.lagou.common;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.nio.CharBuffer;
import java.util.List;

/**
 * @program: rpc-common
 * @description:
 * @author: funfish
 * @create: 2020-11-22 14:20
 **/
public class RpcDecoder extends ByteToMessageDecoder {

    private Class<?> clazz;

    private Serializer serializer;

    public static final int HEAD_LENGTH = 4;

    public RpcDecoder(Class<?> clazz, Serializer serializer) {
        this.clazz = clazz;
        this.serializer = serializer;
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {

          //我们标记一下当前的readIndex的位置
        int dataLength = byteBuf.readInt();       // 读取传送过来的消息
        System.out.printf("dataLength"+dataLength);// 的长度。ByteBuf 的readInt()方法会让他的readIndex增加4

        byte[] body = new byte[dataLength];  //  嗯，这时候，我们读到的长度，满足我们的要求了，把传送过来的数据，取出来吧~~
        byteBuf.readBytes(body);  //

        Object deserialize = serializer.deserialize(clazz, body);
        list.add(deserialize);

    }
}
