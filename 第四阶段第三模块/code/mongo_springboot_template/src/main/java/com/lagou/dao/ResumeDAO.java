package com.lagou.dao;

import com.lagou.bean.Resume;

import java.util.List;

public interface ResumeDAO {
    void  insertResume(Resume resume);
    /** 根据name 获取Resume 对象 */
    Resume  findByName(String name);
    List<Resume> findList(String name);
    /** 根据name  和  expectSalary 查询 */
    List<Resume> findListByNameAndExpectSalary(String name, double expectSalary);

}
