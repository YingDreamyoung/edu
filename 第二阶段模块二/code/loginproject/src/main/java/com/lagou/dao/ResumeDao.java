package com.lagou.dao;

import com.lagou.pojo.Resume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * ⼀个符合SpringDataJpa要求的Dao层接⼝是需要继承JpaRepository和
 JpaSpecificationExecutor
 *
 * JpaRepository<操作的实体类类型,主键类型>
 * 封装了基本的CRUD操作
 *
 * JpaSpecificationExecutor<操作的实体类类型>
 * 封装了复杂的查询（分⻚、排序等）
 *
 */
public interface ResumeDao extends JpaRepository<Resume,Long>, JpaSpecificationExecutor<Resume> {


    @Query("from Resume where id=?1 and name=?2")
    public List<Resume> findByJpql(Long id, String name);
/**
 * 使⽤原⽣sql语句查询，需要将nativeQuery属性设置为true，默认为false（jpql）
 * @param name
 * @param address
 * @return
 */
    @Query(value = "select * from tb_resume where name like ?1 and address like ?2",nativeQuery = true)
            public List<Resume> findBySql(String name, String address);
    /**
     * ⽅法命名规则查询
     * 按照name模糊查询（like）
     * ⽅法名以findBy开头
     * -属性名（⾸字⺟⼤写）
     * -查询⽅式（模糊查询、等价查询），如果不写查询⽅式，默认等价
     查询
     */
    public List<Resume> findByNameLikeAndAddress(String name, String address);
}
