package com.lagou.controller;

import com.lagou.pojo.Resume;
import com.lagou.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("demo")
public class DemoController {

    @Autowired
    private ResumeService resumeService;

    @RequestMapping("/result")
    public String result(){
        return "result";
    }

    @RequestMapping("/resultList")
    @ResponseBody
    public ModelAndView login(@RequestParam("name") String name, @RequestParam("password") String password) throws Exception {

        List<Resume> list = resumeService.queryAll();
        ModelAndView modelAndView = new ModelAndView();
        // addObject 其实是向请求域中request.setAttribute("date",date);
        modelAndView.addObject("list",list);
        // 视图信息(封装跳转的页面信息) 逻辑视图名
        modelAndView.setViewName("list");

        return modelAndView;
    }
}
