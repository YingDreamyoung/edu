package com.lagou.service.impl;

import com.lagou.dao.ResumeDao;
import com.lagou.pojo.Resume;
import com.lagou.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeDao resumeDao;

    @Override
    public List<Resume> queryAll() throws Exception {
        return resumeDao.findAll();
    }

    @Override
    public Resume findOne(Long id) throws Exception {

        Resume resume = resumeDao.findById(id).get();
        return resume;
    }

    @Override
    public void add(Resume resume) throws Exception {
        resumeDao.save(resume);
    }
    @Override
    public void del(Resume resume) throws Exception {
        resumeDao.delete(resume);
    }

    @Override
    public void update(Resume resume) throws Exception {
        resumeDao.save(resume);
    }

}
