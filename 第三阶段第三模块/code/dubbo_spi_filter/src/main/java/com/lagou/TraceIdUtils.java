package com.lagou;

/**
 * @program: dubbo_spi_filter
 * @description:
 * @author: funfish
 * @create: 2020-12-01 19:50
 **/
public class TraceIdUtils {

    private static final ThreadLocal<String> TRACE_ID_THREAD_LOCAL=new ThreadLocal<>();
    public  static String getTraceId(){
        return TRACE_ID_THREAD_LOCAL.get();
    }

    public static void setTraceId(String traceId){
        TRACE_ID_THREAD_LOCAL.set(traceId);
    }

    public static void clear(){
        TRACE_ID_THREAD_LOCAL.remove();
    }
}
