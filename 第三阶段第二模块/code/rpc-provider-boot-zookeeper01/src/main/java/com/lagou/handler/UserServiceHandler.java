package com.lagou.handler;

import com.lagou.common.CustBeanFactory;
import com.lagou.common.RpcRequest;
import com.lagou.service.IUserService;
import com.lagou.service.UserServiceImpl;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 自定义的业务处理器
 */
@Slf4j
@Component
@ChannelHandler.Sharable
public class UserServiceHandler extends ChannelInboundHandlerAdapter {

    @Autowired
    CustBeanFactory custBeanFactory;
    //当客户端读取数据时,该方法会被调用
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object rpcObj) throws Exception {

        //注意:  客户端将来发送请求的时候会传递一个参数:  UserService#sayHello#are you ok
        //1.判断当前的请求是否符合规则
//        if(rpcObj.toString().startsWith("UserService")){
//            //2.如果符合规则,调用实现类货到一个result
//            UserServiceImpl service = new UserServiceImpl();
//            String result = service.sayHello(rpcObj.toString().substring(rpcObj.toString().lastIndexOf("#")+1));
//            //3.把调用实现类的方法获得的结果写到客户端
//            ctx.writeAndFlush(result);
//            return;
//        }
        RpcRequest rpc = (RpcRequest) rpcObj;
        log.info("=======> RpcRequest:{}",rpc);
        //1.创建代理对象s
        String className = rpc.getClassName();
        Class<?> beanClass = Class.forName(className);
        Object bean = custBeanFactory.getBean(beanClass);
        String methodName = rpc.getMethodName();
        Object[] parameters = rpc.getParameters();
        Class<?>[] parameterTypes = rpc.getParameterTypes();
        String requestId = rpc.getRequestId();

        log.info("=======> requestId:{}",requestId);

        Method[] declaredMethods = beanClass.getDeclaredMethods();
        for (Method method : declaredMethods) {
            if(methodName.equalsIgnoreCase(method.getName())){
                Object invoke = method.invoke(bean, parameters);
                ctx.writeAndFlush(invoke);
            }

        }


//        IUserService service = (IUserService) RPCConsumer.createProxy(IUserService.class, PROVIDER_NAME);


    }
}
