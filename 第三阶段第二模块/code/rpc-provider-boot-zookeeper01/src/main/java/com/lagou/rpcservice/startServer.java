package com.lagou.rpcservice;

import com.lagou.common.*;
import com.lagou.handler.UserServiceHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @program: rpcservice-provider-boot
 * @description:
 * @author: funfish
 * @create: 2020-11-29 15:28
 **/
@Component
public class startServer {

    @Autowired
    CustBeanFactory custBeanFactory;

    //创建一个方法启动服务器
    @PostConstruct
    public void startServer() throws InterruptedException {

        //1.创建两个线程池对象
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workGroup = new NioEventLoopGroup();

        //2.创建服务端的启动引导对象
        ServerBootstrap serverBootstrap = new ServerBootstrap();

        //3.配置启动引导对象
        serverBootstrap.group(bossGroup, workGroup)
                //设置通道为NIO
                .channel(NioServerSocketChannel.class)
                //创建监听channel
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        //获取管道对象
                        ChannelPipeline pipeline = nioSocketChannel.pipeline();
                        //给管道对象pipeLine 设置编码
                        pipeline.addLast(new StringEncoder());
//                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(new RpcDecoder(RpcRequest.class, new JSONSerializer()));
                        //把我们自顶一个ChannelHander添加到通道中
//                        pipeline.addLast( new UserServiceHandler());
                        pipeline.addLast(custBeanFactory.getBean(UserServiceHandler.class));
                    }
                });

        //4.绑定端口
        serverBootstrap.bind(8999).sync();

        // zk 管理
        registzk();

    }

    private void registzk() {

        /*
            创建一个zkclient实例就可以完成连接，完成会话的创建
            serverString : 服务器连接地址

            注意：zkClient通过对zookeeperAPI内部封装，将这个异步创建会话的过程同步化了..
         */

        ZkClient zkClient = new ZkClient("127.0.0.1:2181");
        zkClient.setZkSerializer(new SerializableSerializer());
        System.out.println("会话被创建了..");

        // 判断节点是否存在
        String path = "/lg-rpc";
        boolean exists = zkClient.exists(path);

        if(!exists){
            // 创建节点
            zkClient.createEphemeral(path,"127.0.0.1:8999");
        }

        // 读取节点内容
        Object o = zkClient.readData(path);
        System.out.println(o);


        // 注册监听
        zkClient.subscribeDataChanges(path, new IZkDataListener() {

            /*
                当节点数据内容发生变化时，执行的回调方法
                s: path
                o: 变化后的节点内容
             */
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                System.out.println(s+"该节点内容被更新，更新的内容"+o);
            }

            /*
                当节点被删除时，会执行的回调方法
                s : path
             */
            @Override
            public void handleDataDeleted(String s) throws Exception {
                System.out.println(s+"该节点被删除");
            }
        });


        // 更新节点内容
//        zkClient.writeData(path,"456");
//        Thread.sleep(1000);

        // 删除节点
//        zkClient.delete(path);
//        Thread.sleep(1000);
    }


}
