package com.lagou;

import com.lagou.service.UserServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpcProviderBootApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(RpcProviderBootApplication.class, args);

    }

}
