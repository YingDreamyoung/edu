package com.lagou.client;

import io.netty.bootstrap.Bootstrap;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @program: rpc-provider-boot
 * @description:
 * @author: funfish
 * @create: 2020-11-29 19:25
 **/
@Component
public class RPCZK extends TimerTask {

    Set<String> zkServices = new HashSet<>(8);

    private  ZkClient zkClient;

    @PostConstruct
    void initRPCZK(){
        zkClient = new ZkClient("127.0.0.1:2181");
//        zkClient.setZkSerializer(new MyZkSerializer());
        zkClient.setZkSerializer(new SerializableSerializer());
        System.out.println("会话被创建了..");
    }

    @Override
    public void run() {

        /*
            创建一个zkclient实例就可以完成连接，完成会话的创建
            serverString : 服务器连接地址

            注意：zkClient通过对zookeeperAPI内部封装，将这个异步创建会话的过程同步化了..
         */

        // 判断节点是否存在
        String path = "/lg-rpc";
        boolean exists = zkClient.exists(path);

        if(!exists){
            return;
            // 创建节点
//            zkClient.createEphemeral(path,"127.0.0.1:8999");
        }

        // 读取节点内容
        Object o = zkClient.readData(path);
        System.out.println(new Date());

        System.out.println(o);
        zkServices.add((String)o);

        // 注册监听
        zkClient.subscribeDataChanges(path, new IZkDataListener() {
            /*
                当节点数据内容发生变化时，执行的回调方法
                s: path
                o: 变化后的节点内容
             */
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                System.out.println(new Date());
                zkServices.add((String)o);
                System.out.println(s+"该节点内容被更新，更新的内容"+o);
            }

            /*
                当节点被删除时，会执行的回调方法
                s : path
             */
            @Override
            public void handleDataDeleted(String s) throws Exception {
                System.out.println(new Date());
                zkServices.remove((String)o);
                System.out.println(s+"该节点被删除");
            }
        });


    }
}
