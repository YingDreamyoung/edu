package com.lagou;

import com.lagou.client.RPCConsumer;
import com.lagou.common.CustBeanFactory;
import com.lagou.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class RpcConsumerBootApplication {

    //参数定义
    private static final String PROVIDER_NAME = "UserService#sayHello#";


    public static void main(String[] args) throws InterruptedException {

        ConfigurableApplicationContext run = SpringApplication.run(RpcConsumerBootApplication.class, args);

        //1.创建代理对象

        //2.循环给服务器写数据

        while (true){
            RPCConsumer rpcConsumer = run.getBean(RPCConsumer.class);
            if(RPCConsumer.bootstrapMap.size()>0){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.printf(Thread.currentThread().getName()+"run....");
                        IUserService service = (IUserService) rpcConsumer.createProxy(IUserService.class, PROVIDER_NAME);
                        String result = service.sayHello("are you ok ??");
                        System.out.println(Thread.currentThread().getName()+  " 收到返回:"+ result);
                    }
                }).start();

            }

             Thread.sleep(5000);

        }

    }

}
