package com.lagou.client;

import com.lagou.common.JSONSerializer;
import com.lagou.common.RpcEncoder;
import com.lagou.common.RpcRequest;
import com.lagou.handler.UserClientHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 消费者
 */

@Component
public class RPCConsumer{

    public static Map<String,Bootstrap> bootstrapMap = new HashMap<String, Bootstrap>();
    private Set<String> zkServices = new HashSet<>(8);

    //1.创建一个线程池对象  -- 它要处理我们自定义事件
    private static ExecutorService executorService =
            Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    //2.声明一个自定义事件处理器  UserClientHandler
//    @Autowired
    private UserClientHandler userClientHandler;

//    @Autowired
//    private RPCZK rpczk;

    private  ZkClient zkClient;

    @PostConstruct
    public void getPool() throws InterruptedException {

        zkClient = new ZkClient("127.0.0.1:2181");
//        zkClient.setZkSerializer(new MyZkSerializer());
        zkClient.setZkSerializer(new SerializableSerializer());
        System.out.println("会话被创建了..");

        // 判断节点是否存在
        String path = "/lg-rpc";
        boolean exists = zkClient.exists(path);

        if(!exists){
            System.out.println("暂无可用无服务 zkServices = " + zkServices);
            return;
            // 创建节点
//            zkClient.createEphemeral(path,"127.0.0.1:8999");
        }

        // 读取节点内容
        Object o = zkClient.readData(path);
        System.out.println(new Date());
        System.out.println(o);
        zkServices.add((String)o);

        if(bootstrapMap.containsKey((String)o)){

        }else {
            String[] args = ((String) o).split(":");
            initClient(args[0],Integer.parseInt(args[1]));
        }

        // 注册监听
        zkClient.subscribeDataChanges(path, new IZkDataListener() {
            /*
                当节点数据内容发生变化时，执行的回调方法
                s: path
                o: 变化后的节点内容
             */
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                System.out.println(new Date());
                zkServices.add((String)o);
                String[] args = ((String) o).split(":");
                initClient(args[0],Integer.parseInt(args[1]));
                System.out.println(s+"该节点内容被更新，更新的内容"+o);
                System.out.printf(bootstrapMap.toString());
            }

            /*
                当节点被删除时，会执行的回调方法
                s : path
             */
            @Override
            public void handleDataDeleted(String s) throws Exception {
                System.out.println(new Date());
                zkServices.remove((String)o);

                bootstrapMap.remove((String)o);
                System.out.println(s+"该节点被删除");

                System.out.printf(bootstrapMap.toString());


            }
        });

        //在3秒后执行MyTask类中的run方法
//        new Timer().schedule(rpczk, 5000,5000);
//        Set<String> zkServices = rpczk.zkServices;
//        if(zkServices ==null || zkServices.isEmpty()){
//            System.out.println("暂无可用无服务 zkServices = " + zkServices);
//        }
        // 判断




    }

    //3.编写方法,初始化客户端  ( 创建连接池  bootStrap  设置bootstrap  连接服务器)
    public void initClient(String ip,int port) throws InterruptedException {

        //1) 初始化UserClientHandler
        userClientHandler = new UserClientHandler();
        //2)创建连接池对象
        EventLoopGroup group = new NioEventLoopGroup();
        //3)创建客户端的引导对象
        Bootstrap bootstrap = new Bootstrap();
        //4)配置启动引导对象
        bootstrap.group(group)
                //设置通道为NIO
                .channel(NioSocketChannel.class)
                //设置请求协议为TCP
                .option(ChannelOption.TCP_NODELAY, true)
                //监听channel 并初始化
                .handler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        //获取ChannelPipeline
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        //设置编码
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
//                        pipeline.addLast(new StringEncoder());
                        pipeline.addLast(new StringDecoder());

                        //添加自定义事件处理器
                        pipeline.addLast(userClientHandler);
                        pipeline.addLast(new IdleStateHandler(5,5,10));
                    }
                });

        //5)连接服务端
        System.out.printf("ip:" +ip +"port:" +port);

//        bootstrap.connect("127.0.0.1", 8999).sync();
        bootstrap.connect(ip, port).sync();
        // 6、 zk 管理远程服务
        bootstrapMap.put(ip+":"+port,bootstrap);

        System.out.println("initClient over!");

    }


    //4.编写一个方法,使用JDK的动态代理创建对象
    // serviceClass 接口类型,根据哪个接口生成子类代理对象;   providerParam :  "UserService#sayHello#"
    public Object createProxy(final Class<?> serviceClass, final String providerParam) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{serviceClass}, new InvocationHandler() {
                    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                        //1)初始化客户端cliet
                        if (userClientHandler == null) {
//                            initClient();
                            getPool();
                        }


                        System.out.println("开始发送:" + objects[0]);

                        //2)给UserClientHandler 设置param参数
                        userClientHandler.setParam(providerParam+objects[0]);

                        RpcRequest rpcRequest = new RpcRequest();
                        rpcRequest.setClassName(serviceClass.getName());
                        rpcRequest.setMethodName(method.getName());
                        rpcRequest.setParameterTypes(new Class[]{objects.getClass()});
                        rpcRequest.setParameters(objects);
                        rpcRequest.setRequestId(UUID.randomUUID().toString());

                        userClientHandler.setRpcRequest(rpcRequest);

                        //3).使用线程池,开启一个线程处理处理call() 写操作,并返回结果
                        Object result = executorService.submit(userClientHandler).get();

                        //4)return 结果
                        return result;
                    }
                });
    }

}
