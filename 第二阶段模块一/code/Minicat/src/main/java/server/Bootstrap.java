package server;

import org.dom4j.*;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Minicat的主类
 */
public class Bootstrap {

    /**定义socket监听的端口号*/
    private int port = 80;

    private String host = "";

    private Map<String, Mapper> serviceMapper= new HashMap();

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    private Map<String,HttpServlet> servletMap = new HashMap<String,HttpServlet>();

    /**
     * Minicat启动需要初始化展开的一些操作
     */
    public void start() throws Exception {

        // 加载解析相关的配置，server.xml
        loadServer();

        // 加载解析相关的配置，web.xml
//        loadServlet();


        // 定义一个线程池
        int corePoolSize = 10;
        int maximumPoolSize =50;
        long keepAliveTime = 100L;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(50);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();


        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                workQueue,
                threadFactory,
                handler
        );





        /*
            完成Minicat 1.0版本
            需求：浏览器请求http://localhost:8080,返回一个固定的字符串到页面"Hello Minicat!"
         */
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("=====>>>Minicat start on port：" + port);

        /*while(true) {
            Socket socket = serverSocket.accept();
            // 有了socket，接收到请求，获取输出流
            OutputStream outputStream = socket.getOutputStream();
            String data = "Hello Minicat!";
            String responseText = HttpProtocolUtil.getHttpHeader200(data.getBytes().length) + data;
            outputStream.write(responseText.getBytes());
            socket.close();
        }*/


        /**
         * 完成Minicat 2.0版本
         * 需求：封装Request和Response对象，返回html静态资源文件
         */
        /*while(true) {
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            response.outputHtml(request.getUrl());
            socket.close();

        }*/


        /**
         * 完成Minicat 3.0版本
         * 需求：可以请求动态资源（Servlet）
         */
        /*while(true) {
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            // 静态资源处理
            if(servletMap.get(request.getUrl()) == null) {
                response.outputHtml(request.getUrl());
            }else{
                // 动态资源servlet请求
                HttpServlet httpServlet = servletMap.get(request.getUrl());
                httpServlet.service(request,response);
            }

            socket.close();

        }
*/

        /*
            多线程改造（不使用线程池）
         */
        /*while(true) {
            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket,servletMap);
            requestProcessor.start();
        }*/



        System.out.println("=========>>>>>>使用线程池进行多线程改造");
        /*
            多线程改造（使用线程池）
         */
        while(true) {

            Socket socket = serverSocket.accept();
//            RequestProcessor requestProcessor = new RequestProcessor(socket,servletMap);
            RequestProcessor requestProcessor = new RequestProcessor(socket,serviceMapper);
            //requestProcessor.start();
            threadPoolExecutor.execute(requestProcessor);
        }

    }

    /**
     *  加载server.xml
     */

//    <Server>
//  <Service>
//    <Connector port="8080" />
//    <Engine>
//      <Host name="localhost"  appBase="/Users/funfish/IdeaProjects/edu/朝花夕拾/第二阶段 Web服务器深度应用及调优/Tomcat&Nginx资料/代码/webapps" />
//    </Engine>
//  </Service>
//</Server>
    private void loadServer() throws FileNotFoundException {

        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            List<Element> selectNodes = rootElement.selectNodes("//Server");
            for (int i = 0; i < selectNodes.size(); i++) {
                //    <Server>
                Element serverElement =  selectNodes.get(i);
                //  <Service>
                Element serviceElement = (Element) serverElement.selectSingleNode("Service");
                    //    <Connector port="8080" />
                    Element connectornameElement = (Element) serviceElement.selectSingleNode("Connector");
                    //    <Connector port="8080" />
                    String portElement = connectornameElement.attributeValue("port");

                    port = Integer.parseInt(portElement);

                    Element engineElement = (Element) serviceElement.selectSingleNode("Engine");

                        Element hostElement = (Element) engineElement.selectSingleNode("Host");

                        String hostAttr =  hostElement.attributeValue("name");

                        host = hostAttr;

                        // 加载web 文件
                        String appBaseAttr =  hostElement.attributeValue("appBase");

                loadServlet(appBaseAttr);

            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        System.out.printf("====>" + serviceMapper);
    }


    /**
     * 加载解析web.xml，初始化Servlet
     */
    private void loadServlet(String appBaseAttr) throws FileNotFoundException {
        // 加载多个web.xml
        File appBase = new File(appBaseAttr);
        if(appBase.isDirectory()){
            File[] files = appBase.listFiles();
            for (File webapp: files) {
               if(webapp.isDirectory()){
                   String absolutePath = webapp.getAbsolutePath();
                   String context = webapp.getName();

                   InputStream resourceAsStream = new FileInputStream(new File(absolutePath + File.separator +"web.xml"));
                   SAXReader saxReader = new SAXReader();
                   try {
                       Document document = saxReader.read(resourceAsStream);
                       Element rootElement = document.getRootElement();

                       List<Element> selectNodes = rootElement.selectNodes("//servlet");
                       for (int i = 0; i < selectNodes.size(); i++) {
                           Element element =  selectNodes.get(i);
                           // <servlet-name>lagou</servlet-name>
                           Element servletnameElement = (Element) element.selectSingleNode("servlet-name");
                           String servletName = servletnameElement.getStringValue();
                           // <servlet-class>server.LagouServlet</servlet-class>
                           Element servletclassElement = (Element) element.selectSingleNode("servlet-class");
                           String servletClass = servletclassElement.getStringValue();
                           // 根据servlet-name的值找到url-pattern
                           Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                           // /lagou
                           String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();

                           Mapper mapper = new Mapper();
                           mapper.setContext(context);
                           mapper.setHost(host);
                           mapper.setWrapper(urlPattern);

                           CustClassLoader custClassLoader = new CustClassLoader();
                           Class<?> aClass = custClassLoader.findClass(absolutePath, servletClass );

                           // 加载Class类
                           mapper.setHttpServlet((HttpServlet)aClass.newInstance());

                           serviceMapper.put("/"+ context + urlPattern,mapper);


//                           servletMap.put(urlPattern, ((HttpServlet) Class.forName(servletClass).newInstance());

                       }
                   } catch (DocumentException e) {
                       e.printStackTrace();
                   } catch (IllegalAccessException e) {
                       e.printStackTrace();
                   } catch (InstantiationException e) {
                       e.printStackTrace();
                   }

               }
            }
        }


//        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("web.xml");
//        SAXReader saxReader = new SAXReader();
//
//        try {
//            Document document = saxReader.read(resourceAsStream);
//            Element rootElement = document.getRootElement();
//
//            List<Element> selectNodes = rootElement.selectNodes("//servlet");
//            for (int i = 0; i < selectNodes.size(); i++) {
//                Element element =  selectNodes.get(i);
//                // <servlet-name>lagou</servlet-name>
//                Element servletnameElement = (Element) element.selectSingleNode("servlet-name");
//                String servletName = servletnameElement.getStringValue();
//                // <servlet-class>server.LagouServlet</servlet-class>
//                Element servletclassElement = (Element) element.selectSingleNode("servlet-class");
//                String servletClass = servletclassElement.getStringValue();
//                // 根据servlet-name的值找到url-pattern
//                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
//                // /lagou
//                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
//                servletMap.put(urlPattern, (HttpServlet) Class.forName(servletClass).newInstance());
//
//            }
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }

    }


    /**
     * Minicat 的程序启动入口
     * @param args
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            // 启动Minicat
            bootstrap.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
