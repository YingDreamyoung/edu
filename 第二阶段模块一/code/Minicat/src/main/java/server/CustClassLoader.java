package server;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @program: Minicat
 * @description:
 * @author: funfish
 * @create: 2020-11-15 01:28
 **/
public class CustClassLoader extends ClassLoader {

    @Override
    protected Class<?> findClass(String filePath,String className) {

        String myPath = filePath + File.separator + className.replace(".","/") + ".class";
        System.out.println(myPath);
        byte[] cLassBytes = null;
        Path path = null;
        try {
            path = Paths.get(new URI("file:"+myPath));
            cLassBytes = Files.readAllBytes(path);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        Class clazz = defineClass(className, cLassBytes, 0, cLassBytes.length);
        return clazz;
    }
}
