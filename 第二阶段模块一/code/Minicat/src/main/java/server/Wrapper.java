package server;

/**
 * @program: Minicat
 * @description:
 * @author: funfish
 * @create: 2020-11-14 22:41
 **/
public class Wrapper {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
