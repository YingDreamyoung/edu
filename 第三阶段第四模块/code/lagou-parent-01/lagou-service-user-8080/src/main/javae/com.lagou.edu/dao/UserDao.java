package com.lagou.edu.dao;

import com.lagou.edu.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 19:30
 **/
public interface UserDao extends JpaRepository<User,Long> {
}
