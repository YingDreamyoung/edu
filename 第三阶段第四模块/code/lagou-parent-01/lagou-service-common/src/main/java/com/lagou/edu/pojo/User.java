package com.lagou.edu.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 19:39
 **/
@Data
@Entity
@Table(name = "lagou_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 主键
    private String email; // 邮箱
    private String password; // 密码
}
