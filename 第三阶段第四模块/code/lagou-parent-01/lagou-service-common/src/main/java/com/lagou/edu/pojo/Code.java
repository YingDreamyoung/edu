package com.lagou.edu.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 19:56
 **/
@Data
@Entity
@Table(name = "lagou_auth_code")
public class Code {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //主键
    private String email; // 邮件
    private String code;// '验证码'
    private Date createtime; //'创建时间'
    private Date expiretime; //'过期时间'

}
