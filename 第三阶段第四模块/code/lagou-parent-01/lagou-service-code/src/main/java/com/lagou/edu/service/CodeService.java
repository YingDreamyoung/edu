package com.lagou.edu.service;

import org.springframework.web.bind.annotation.PathVariable;

public interface CodeService {
    Boolean create(String email);

    Integer validate(String email, String code);
}
