package com.lagou.edu.controller;

import com.lagou.edu.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 22:24
 **/

@RestController
@RequestMapping("/code")
public class CodeController {

    @Autowired
    private CodeService codeService;

    @GetMapping("/create/{email}")
    public Boolean create(@PathVariable String email){

//        ⽣成验证码并发送到对应邮箱，成功true，失败
//        false
        return codeService.create(email);
    }

    @GetMapping("/validate/{email}/{code}")
    public Integer validate(@PathVariable("email") String email,@PathVariable("code") String code){

        return  codeService.validate(email, code);

    }

}
