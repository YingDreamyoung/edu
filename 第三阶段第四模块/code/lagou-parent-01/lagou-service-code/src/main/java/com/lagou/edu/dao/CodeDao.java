package com.lagou.edu.dao;

import com.lagou.edu.pojo.Code;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 20:03
 **/
public interface CodeDao extends JpaRepository<Code,Long> {
}
