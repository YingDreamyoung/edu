package com.lagou.edu.utils;

import org.apache.commons.mail.HtmlEmail;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 22:15
 **/
public class EmailUtil {

    //邮箱验证码
    public static boolean sendEmail(String emailAddress,String code) {
        try {
            HtmlEmail email = new HtmlEmail();//不用更改
            email.setHostName("smtp.163.com");//需要修改，126邮箱为smtp.126.com,163邮箱为163.smtp.com，QQ为smtp.qq.com
            email.setCharset("UTF-8");
            email.addTo(emailAddress);// 收件地址

            email.setFrom("wan_ying163@163.com", "*****");//此处填邮箱地址和用户名,用户名可以任意填写

            email.setAuthentication("wan_ying163@163.com", "wan163");//此处填写邮箱地址和客户端授权码

            email.setSubject("注册验证码");//此处填写邮件名，邮件名可任意填写
            email.setMsg("尊敬的用户您好,您本次注册的验证码是:\n" + code);//此处填写邮件内容
//            email.setSSLOnConnect(false);
            //启用ssl加密
            email.setSSLOnConnect(true);
               //使用465端口(不设置也可，ssl默认为465)
            email.setSslSmtpPort("465");
            email.send();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
