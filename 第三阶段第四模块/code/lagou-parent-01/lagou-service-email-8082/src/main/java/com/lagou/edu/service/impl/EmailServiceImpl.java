package com.lagou.edu.service.impl;

import com.lagou.edu.service.EmailService;
import com.lagou.edu.utils.EmailUtil;
import org.springframework.stereotype.Service;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 22:05
 **/
@Service
public class EmailServiceImpl implements EmailService {

     @Override
     public Boolean sendEmail(String email, String code){

         return EmailUtil.sendEmail(email,code);
     }
}
