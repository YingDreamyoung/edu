<%@ page language="java" isELIgnored="false" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Insert title here</title>
    <%--<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>--%>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $(".submitBtn").bind("click",function(){

                alert("sorry,~");
                var name = $("#name").val();
                var password = $("#password").val();

                if(name == null || $.trim(name).length == 0){
                    alert("sorry,必须输入登录名~");
                    return;
                }
                if(password == null || $.trim(password).length == 0){
                    alert("sorry,必须输入密码~");
                    return;
                }

                $.ajax({
                    url:'/login',
                    type:'POST',    //GET
                    async:false,    //或false,是否异步
                    data:{
                        name:name,
                        password:password,
                    },
                    timeout:5000,    //超时时间
                    dataType:'json', //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){
                        if("200" == data.status){
                            alert("登录成功~~~");
                        }else{
                            alert("登录失败~~~,message:" + data.message);
                        }
                    }
                })
            })
        })
    </script>

</head>
<body>
<%--异常信息: ${msg}--%>

<form>
    <table class="lp-login">

            <tr>
                <td align="right"><span>账户</span></td>
                <td align="center">
                    <input type="text" id="name" value="${name}"></input>
                </td>
                <td align="center">
                    <input type="text" id="password" value="${password}"></input>
                </td>
            </tr>

        <tr align="center">
            <td colspan="2">
                <input type="button" class="submitBtn" id="ajaxBtn" value="登录"/>
            </td>
        </tr>
    </table>
</form>

</body>
</html>
