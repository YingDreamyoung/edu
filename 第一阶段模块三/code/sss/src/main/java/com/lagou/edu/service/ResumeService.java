package com.lagou.edu.service;

import com.lagou.edu.pojo.Resume;

import java.util.List;

public interface ResumeService {

    List<Resume> queryAll() throws Exception;

    Resume findOne(Long id) throws Exception;

    void add(Resume resume) throws Exception;

    void del(Resume resume) throws Exception;

    void update(Resume resume) throws Exception;
}
