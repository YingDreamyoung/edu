package com.lagou.edu.controller;

import com.lagou.edu.pojo.Resume;
import com.lagou.edu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
@RequestMapping("/")
public class ResumeController {

    /**
     * Spring容器和SpringMVC容器是有层次的（父子容器）
     * Spring容器：service对象+dao对象
     * SpringMVC容器：controller对象，，，，可以引用到Spring容器中的对象
     */

    @Autowired
    private ResumeService resumeService;


    @RequestMapping("/login")
    @ResponseBody
    public ModelAndView login(@RequestParam("name") String name,@RequestParam("password") String password) throws Exception {


        List<Resume> list = resumeService.queryAll();
        ModelAndView modelAndView = new ModelAndView();
        // addObject 其实是向请求域中request.setAttribute("date",date);
        modelAndView.addObject("list",list);
        // 视图信息(封装跳转的页面信息) 逻辑视图名
        modelAndView.setViewName("list");

        return modelAndView;
    }


    @RequestMapping("/findOne")
    @ResponseBody
    public Resume findOne(Long id) throws Exception {
        return resumeService.findOne(id);
    }

    @RequestMapping("/add")
    @ResponseBody
    public void findOne(Resume resume) throws Exception {
        resumeService.add(resume);
    }

    @RequestMapping("/del")
    @ResponseBody
    public void del(Resume resume) throws Exception {
        resumeService.del(resume);
    }

    @RequestMapping("/update")
    @ResponseBody
    public void update(Resume resume) throws Exception {
        resumeService.update(resume);
    }

}
