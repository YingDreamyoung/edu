package com.lagou.mapper;

import com.lagou.pojo.Article;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ArticleMapper {

    //根据id查询对应的文章
    public Article selectArticle(Integer id);

    public List<Article> selectArticlePage(@Param("pageNoIndex") Integer pageNoIndex,@Param("pageSize") Integer pageSize);
    // 查询总页数
    public Integer selectArticleTotalNum();



}
