package com.lagou.service.impl;

import com.lagou.mapper.ArticleMapper;
import com.lagou.pojo.Article;
import com.lagou.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @program: springboot03_data
 * @description:
 * @author: funfish
 * @create: 2020-11-08 12:09
 **/
@Service
public class ArticleServiceImpl implements IArticleService {

    private static final Integer defPageSize = 2;


    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Article> getArticlePage(Integer pageNo, Integer pageSize) {

       return articleMapper.selectArticlePage((pageNo -1)* defPageSize, defPageSize);

    }

    @Override
    public Integer getArticleTotalPage() {
        Integer totalPage = (articleMapper.selectArticleTotalNum() + defPageSize - 1) / defPageSize;
        return totalPage;
    }
}
