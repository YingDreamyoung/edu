package com.lagou.controller;

import com.lagou.pojo.Article;
import com.lagou.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class LoginController {

    @Autowired
    private IArticleService articleService;

    @RequestMapping("/")
    public ModelAndView index(Model model){

        ModelAndView modelAndView = new ModelAndView();

        List<Article> articlePage = articleService.getArticlePage(1, 2);

        Integer totalNum = articleService.getArticleTotalPage();

        modelAndView.addObject("articleList",articlePage);
        modelAndView.addObject("totalPage",totalNum);
        modelAndView.addObject("currentPageNo",1);

        modelAndView.setViewName("index");

        return modelAndView;
    }


    @RequestMapping("/page")
    public ModelAndView index(Integer pageNo){

        ModelAndView modelAndView = new ModelAndView();

        List<Article> articlePage = articleService.getArticlePage(pageNo, 2);

        Integer totalPage = articleService.getArticleTotalPage();

        modelAndView.addObject("articleList",articlePage);
        modelAndView.addObject("totalPage",totalPage);
        modelAndView.addObject("currentPageNo",pageNo);

        modelAndView.setViewName("index");

        return modelAndView;
    }


}
