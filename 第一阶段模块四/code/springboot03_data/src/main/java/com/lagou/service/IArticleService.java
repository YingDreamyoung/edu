package com.lagou.service;

import com.lagou.pojo.Article;

import java.util.List;

public interface IArticleService {
    List<Article> getArticlePage(Integer pageNo, Integer pageSize);

    Integer getArticleTotalPage();
}
