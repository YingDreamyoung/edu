package com.lagou.edu.service.impl;

import com.lagou.edu.dao.CodeDao;
import com.lagou.edu.pojo.Code;
import com.lagou.edu.pojo.User;
import com.lagou.edu.service.CodeService;
import com.lagou.edu.service.feign.EmailServiceFeignClient;
import com.lagou.edu.service.feign.UserServiceFeignClient;
import com.lagou.edu.utils.CodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.Date;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 22:34
 **/
@Service
@Slf4j
public class CodeServiceImpl implements CodeService {

    @Value("${app.code.ttl}")
    private Long ttl;

    @Autowired
    private CodeDao codeDao;

    @Autowired
    private EmailServiceFeignClient emailServiceFeignClient;

    @Autowired
    private UserServiceFeignClient userServiceFeignClient;

    @Override
    public Boolean create(@PathVariable String email){

        /**
         * ⽣成验证码并发送到对应邮箱，成功true，失败
         * false
         */
        Boolean registered = userServiceFeignClient.isRegistered(email);
        if(registered){
            log.error("======>>>>> email:{} has registered",email);
            return false;
        }
        String code = CodeUtil.getCode(4);
        Boolean emailBoolean = emailServiceFeignClient.email(email, code);
        if(emailBoolean){
            Code db = new Code();
            db.setCode(code);
            db.setEmail(email);
            db.setCreatetime(new Date());

            db.setExpiretime(new Date(System.currentTimeMillis()  + ttl* 1000));
            codeDao.save(db);
            return true;
        }

        return false;
    }



    /**
     * 校验验证码是否正确，0正确1错误2超时
     * @param email
     * @param code
     * @return
     */
    @Override
    public Integer validate(String email, String code){

        Code db = new Code();
        db.setCode(code);
        db.setEmail(email);
        Example<Code> example = Example.of(db);
        boolean exists = codeDao.exists(example);

        return exists ? 0 :1 ;
    }
}
