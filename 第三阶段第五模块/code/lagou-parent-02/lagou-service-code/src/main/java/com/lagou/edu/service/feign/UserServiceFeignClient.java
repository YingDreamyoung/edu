package com.lagou.edu.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 22:44
 **/
@FeignClient(value = "lagou-service-user",path = "/user")
public interface UserServiceFeignClient {

    @GetMapping("/isRegistered/{email}")
    public Boolean isRegistered(@PathVariable("email") String email);
}
