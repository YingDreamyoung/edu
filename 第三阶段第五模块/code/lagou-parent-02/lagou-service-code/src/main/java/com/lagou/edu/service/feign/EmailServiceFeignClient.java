package com.lagou.edu.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 22:44
 **/
@FeignClient(value = "lagou-service-email",path = "/email")
public interface EmailServiceFeignClient {

    @GetMapping("/{email}/{code}")
    public Boolean email(@PathVariable("email") String email, @PathVariable("code") String code);
}
