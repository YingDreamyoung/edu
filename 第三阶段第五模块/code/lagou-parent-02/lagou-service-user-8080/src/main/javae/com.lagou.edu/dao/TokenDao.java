package com.lagou.edu.dao;

import com.lagou.edu.pojo.Token;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 20:18
 **/
public interface TokenDao extends JpaRepository<Token,Long> {
}
