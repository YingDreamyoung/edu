package com.lagou.edu.service;

import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 20:30
 **/
public interface UserService {


    public Integer register(String email, String password, String code);

    public boolean isRegistered(String email);

    public String login(String email, String password, HttpServletResponse response);

    public String info(String token);
}
