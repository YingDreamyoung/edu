package com.lagou.edu.service.impl;

import com.lagou.edu.dao.TokenDao;
import com.lagou.edu.dao.UserDao;
import com.lagou.edu.pojo.Token;
import com.lagou.edu.pojo.User;
import com.lagou.edu.service.UserService;
import com.lagou.edu.service.feign.CodeServiceFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 20:35
 **/
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private TokenDao tokenDao;

    @Autowired
    private CodeServiceFeignClient codeServiceFeignClient;

    /**
     * 注册接⼝，true成功，false失败
     */
    @Override
    public Integer register(String email, String password, String code) {

        // 检验是否已注册
        boolean registered = this.isRegistered(email);
        if(registered){
            log.error("======>>>>> email:{} has registered",email);
            return 1;
        }
        // code 是否正确
        Integer validate = codeServiceFeignClient.validate(email, code);
        log.info("=======>>>>>validate :{}",validate);
        if(validate == 0){
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            Example<User> example = Example.of(user);
            User save = userDao.save(user);
            log.info("========>>>>> save :{}",save);
            return 0;
        }
        return 1;
    }

    /**
     * 是否已注册，根据邮箱判断,true代表已经注册过，
     * false代表尚未注册
     * @param email
     * @return
     */
    @Override
    public boolean isRegistered(String email) {

        User user = new User();
        user.setEmail(email);
        Example<User> example = Example.of(user);

        return userDao.findOne(example).isPresent();

    }

    /**
     * 登录接⼝，验证⽤户名密码合法性，根据⽤户名和
     * 密码⽣成token，token存⼊数据库，并写⼊cookie
     * 中，登录成功返回邮箱地址，重定向到欢迎⻚
     * @param email
     * @param password
     */
    @Override
    public String login(String email, String password, HttpServletResponse response) {

//        登录接⼝，验证⽤户名密码合法性，根据⽤户名和
//        密码⽣成token，token存⼊数据库，并写⼊cookie
//        中，登录成功返回邮箱地址，重定向到欢迎⻚
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        Example<User> example = Example.of(user);
        User dbUser = userDao.findOne(example).get();
        if(dbUser != null){

            String token = UUID.randomUUID().toString().replace("-","");
            Token db = new Token();
            db.setToken(token);
            db.setEmail(email);
            Token save = tokenDao.save(db);
            log.info("========>>>>>> Token save:{}",save);

            Cookie cookie = new Cookie("token",token);
            cookie.setPath("/");
            response.addCookie(cookie);

            return email;
        }

        return null;

    }

    /**
     * 根据token查询⽤户登录邮箱接⼝
     * @param token
     */
    @Override
    public String info(String token) {

        Token reqdb = new Token();
        reqdb.setToken(token);
        Example<Token> example = Example.of(reqdb);
        Token db = tokenDao.findOne(example).get();
        if(db != null){
            return db.getEmail();
        }

        return "";

    }
}
