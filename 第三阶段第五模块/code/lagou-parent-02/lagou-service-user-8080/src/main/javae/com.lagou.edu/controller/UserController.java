package com.lagou.edu.controller;

import com.lagou.edu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 19:29
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/register/{email}/{password}/{code}")
    public Integer register(@PathVariable("email") String email, @PathVariable("password") String password, @PathVariable("code") String code){

       return userService.register(email,password,code);
    }

    @GetMapping("/isRegistered/{email}")
    public Boolean isRegistered(@PathVariable String email){

       return userService.isRegistered(email);
    }

    @GetMapping("/login/{email}/{password}")
    public String login(@PathVariable("email") String email, @PathVariable("password") String password, HttpServletResponse httpServletResponse){

         String login = userService.login(email, password, httpServletResponse);
        return  login;

    }

    @GetMapping("/info/{token}")
    public String info(@PathVariable String token){

        return userService.info(token);
    }
}
