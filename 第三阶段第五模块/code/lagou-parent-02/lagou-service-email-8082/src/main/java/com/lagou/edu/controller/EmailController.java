package com.lagou.edu.controller;

import com.lagou.edu.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 22:05
 **/
@RestController
@RequestMapping("/email")
@RefreshScope
public class EmailController {

    @Autowired
    private EmailService emailService;

    @Value("${lagou.message}")
    private String msg;

    @GetMapping("/{email}/{code}")
    public Boolean email(@PathVariable String email,@PathVariable String code){

        return emailService.sendEmail(email,code);
    }

    @GetMapping("/config")
    public String config(){

        return this.msg;
    }
}
