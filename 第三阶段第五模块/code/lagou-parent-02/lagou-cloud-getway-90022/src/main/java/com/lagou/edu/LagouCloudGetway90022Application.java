package com.lagou.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class LagouCloudGetway90022Application {

    public static void main(String[] args) {
        SpringApplication.run(LagouCloudGetway90022Application.class, args);
    }

}
