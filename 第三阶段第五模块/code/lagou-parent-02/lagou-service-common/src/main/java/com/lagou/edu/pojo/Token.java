package com.lagou.edu.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @program: lagou-parent-01
 * @description:
 * @author: funfish
 * @create: 2020-12-04 20:09
 **/
@Data
@Entity
@Table(name = "lagou_token")
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String token;

}
